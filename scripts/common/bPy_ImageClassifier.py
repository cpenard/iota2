#!/usr/bin/python
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import argparse,shutil,os,Sensors
from config import Config
import otbApplication as otb
import fileUtils as fu
from Utils import Opath
        
def filterOTB_output(raster,mask,output,outputType=otb.ImagePixelType_uint8):
        
        bandMathFilter = otb.Registry.CreateApplication("BandMath")
        bandMathFilter.SetParameterString("exp","im2b1==1?im1b1:0")
        bandMathFilter.SetParameterStringList("il",[raster,mask])
        bandMathFilter.SetParameterString("ram","10000")
        bandMathFilter.SetParameterString("out",output,"?&streaming:type=stripped&streaming:sizemode=nbsplits&streaming:sizevalue=10")
        if outputType : bandMathFilter.SetParameterOutputImagePixelType("out",outputType)
        bandMathFilter.ExecuteAndWriteOutput()

def launchClassification(tempFolderSerie,Classifmask,model,stats,outputClassif,confmap,pathWd,pathConf,pixType,MaximizeCPU="False"):
	
	os.environ["ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS"] = "5"
	featuresPath = Config(file(pathConf)).chain.featuresPath
	outputPath = Config(file(pathConf)).chain.outputPath
	outFeatures = Config(file(pathConf)).GlobChain.features
	tile = outputClassif.split("/")[-1].split("_")[1]
	userFeatPath = Config(file(pathConf)).chain.userFeatPath
  	if userFeatPath == "None" : userFeatPath = None
	extractBands = Config(file(pathConf)).iota2FeatureExtraction.extractBands
    	if extractBands == "False" : extractBands = None
        if MaximizeCPU == "True": 
                MaximizeCPU = True
        else :
                MaximizeCPU = False

	AllRefl = sorted(fu.FileSearch_AND(featuresPath+"/"+tile+"/tmp/",True,"REFL.tif"))
        AllMask = sorted(fu.FileSearch_AND(featuresPath+"/"+tile+"/tmp/",True,"MASK.tif"))
        datesInterp = sorted(fu.FileSearch_AND(featuresPath+"/"+tile+"/tmp/",True,"DatesInterp"))
        realDates = sorted(fu.FileSearch_AND(featuresPath+"/"+tile+"/tmp/",True,"imagesDate"))

	tmpFolder = outputPath+"/TMPFOLDER_"+tile
   	S2 = Sensors.Sentinel_2("",Opath(tmpFolder,create = False),pathConf,"",createFolder = None)
    	L8 = Sensors.Landsat8("",Opath(tmpFolder,create = False),pathConf,"",createFolder = None)
    	L5 = Sensors.Landsat5("",Opath(tmpFolder,create = False),pathConf,"",createFolder = None)

    	SensorsList = [S2,L8,L5]
        #gapFill + feat
        features = []
        concatSensors= otb.Registry.CreateApplication("ConcatenateImages")
        for refl,mask,datesInterp,realDates in zip(AllRefl,AllMask,datesInterp,realDates):

	    currentSensor = fu.getCurrentSensor(SensorsList,refl)

	    nbDate = fu.getNbDateInTile(realDates)
	    gapFill = otb.Registry.CreateApplication("ImageTimeSeriesGapFilling")
	    nbReflBands = fu.getRasterNbands(refl)
            comp = int(nbReflBands)/int(nbDate)
	    print datesInterp
            if not isinstance( comp, int ):
                raise Exception("unvalid component by date (not integer) : "+comp)
            
            gapFill.SetParameterString("mask",mask)
            gapFill.SetParameterString("it","linear")
            gapFill.SetParameterString("id",realDates)
            gapFill.SetParameterString("od",datesInterp)
	    
	    if extractBands :
		bandsToKeep = [bandNumber for bandNumber,bandName in currentSensor.keepBands]
	    	extract = fu.ExtractInterestBands(refl,nbDate,bandsToKeep,comp,ram = 10000)
		comp = len(bandsToKeep)
		gapFill.SetParameterInputImage("in",extract.GetParameterOutputImage("out"))
	    else : gapFill.SetParameterString("in",refl)
	    gapFill.SetParameterString("comp",str(comp))
            gapFill.Execute()

            featExtr = otb.Registry.CreateApplication("iota2FeatureExtraction")
            featExtr.SetParameterInputImage("in",gapFill.GetParameterOutputImage("out"))
            featExtr.SetParameterString("comp",str(comp))

	    red = str(currentSensor.bands["BANDS"]["red"])
	    nir = str(currentSensor.bands["BANDS"]["NIR"])
	    swir = str(currentSensor.bands["BANDS"]["SWIR"])
	    if extractBands : 
		red = str(fu.getIndex(currentSensor.keepBands,"red"))
		nir = str(fu.getIndex(currentSensor.keepBands,"NIR"))
		swir = str(fu.getIndex(currentSensor.keepBands,"SWIR"))

            featExtr.SetParameterString("red",red)
            featExtr.SetParameterString("nir",nir)
            featExtr.SetParameterString("swir",swir)
	    featExtr.SetParameterString("ram","256")
	    fu.iota2FeatureExtractionParameter(featExtr,pathConf)
	    if not outFeatures :
		print "without Features"
	    	concatSensors.AddImageToParameterInputImageList("il",gapFill.GetParameterOutputImage("out"))
		features.append(gapFill)
	    else:
		print "with Features"
		featExtr.Execute()
		features.append(featExtr)
	    	concatSensors.AddImageToParameterInputImageList("il",featExtr.GetParameterOutputImage("out"))
            
	classifier = otb.Registry.CreateApplication("ImageClassifier")
	if not MaximizeCPU : classifier.SetParameterString("mask",Classifmask)
	if stats : classifier.SetParameterString("imstat",stats)
	classifier.SetParameterString("model",model)
	classifier.SetParameterString("ram","5120")
	print "AllRefl"
	print AllRefl
	if len(AllRefl) > 1:
		concatSensors.Execute()
		allFeatures = concatSensors.GetParameterOutputImage("out")
	else : allFeatures = features[0].GetParameterOutputImage("out")

	if userFeatPath :
		print "Add user features"
		userFeat_arbo = Config(file(pathConf)).userFeat.arbo
		userFeat_pattern = (Config(file(pathConf)).userFeat.patterns).split(",")
		concatFeatures = otb.Registry.CreateApplication("ConcatenateImages")
		userFeatures = fu.getUserFeatInTile(userFeatPath,tile,userFeat_arbo,userFeat_pattern)
		concatFeatures.SetParameterStringList("il",userFeatures)
		concatFeatures.Execute()

		concatAllFeatures = otb.Registry.CreateApplication("ConcatenateImages")
		concatAllFeatures.AddImageToParameterInputImageList("il",allFeatures)
		concatAllFeatures.AddImageToParameterInputImageList("il",concatFeatures.GetParameterOutputImage("out"))
		concatAllFeatures.Execute()

		allFeatures = concatAllFeatures.GetParameterOutputImage("out")

	classifier.SetParameterInputImage("in",allFeatures)
        classifier.SetParameterString("out",outputClassif)
	classifier.SetParameterOutputImagePixelType("out",otb.ImagePixelType_uint8)
        classifier.SetParameterString("confmap",confmap)
        classifier.ExecuteAndWriteOutput()

        if MaximizeCPU :
                filterOTB_output(outputClassif,Classifmask,outputClassif)
                filterOTB_output(confmap,Classifmask,confmap)

	if pathWd : shutil.copy(outputClassif,outputPath+"/classif")
	if pathWd : shutil.copy(confmap,outputPath+"/classif")
if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = "Performs a classification of the input image (compute in RAM) according to a model file, ")
	parser.add_argument("-in",dest = "tempFolderSerie",help ="path to the folder which contains temporal series",default=None,required=True)
	parser.add_argument("-mask",dest = "mask",help ="path to classification's mask",default=None,required=True)
	parser.add_argument("-pixType",dest = "pixType",help ="pixel format",default=None,required=True)
	parser.add_argument("-model",dest = "model",help ="path to the model",default=None,required=True)
	parser.add_argument("-imstat",dest = "stats",help ="path to statistics",default=None,required=False)
	parser.add_argument("-out",dest = "outputClassif",help ="output classification's path",default=None,required=True)
	parser.add_argument("-confmap",dest = "confmap",help ="output classification confidence map",default=None,required=True)
	parser.add_argument("-ram",dest = "ram",help ="pipeline's size",default=128,required=False)	
	parser.add_argument("--wd",dest = "pathWd",help ="path to the working directory",default=None,required=False)
	parser.add_argument("-conf",help ="path to the configuration file (mandatory)",dest = "pathConf",required=True)
	parser.add_argument("-maxCPU",help ="True : Class all the image and after apply mask",\
                            dest = "MaximizeCPU",default = "False",choices = ["True","False"],required=False)
	args = parser.parse_args()

	launchClassification(args.tempFolderSerie,args.mask,args.model,args.stats,args.outputClassif,\
                             args.confmap,args.pathWd,args.pathConf,args.pixType,args.MaximizeCPU)







